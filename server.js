const path = require("path");
const express = require("express");
const app = express();
const server = require("http").Server(app);
const io = require("socket.io")(server);
const jwt = require("jsonwebtoken");
const passport = require("passport");
const bodyParser = require("body-parser");
const users = require("./users.json");
const texts = require("./texts.json");

require("./passport.config");

server.listen(3000);

app.use(express.static(path.join(__dirname, "public")));
app.use(passport.initialize());
app.use(bodyParser.json());

app.get("/", function(req, res) {
  res.sendFile(path.join(__dirname, "index.html"));
});

app.get(
  "/game",
  /*passport.authenticate('jwt',{session:false}),*/ function(req, res) {
    res.sendFile(path.join(__dirname, "game.html"));
  }
);

app.get("/text", passport.authenticate("jwt", { session: false }), function(
  req,
  res
) {
  let textsLength = texts.length;
  let text = texts[Math.floor(Math.random() * textsLength)];
  res.send(text.text);
});

app.get("/login", function(req, res) {
  res.sendFile(path.join(__dirname, "login.html"));
});

app.post("/login", function(req, res) {
  const userFromReq = req.body;
  const userInDB = users.find(user => user.login === userFromReq.login);
  if (userInDB && userInDB.password === userFromReq.password) {
    const token = jwt.sign(userFromReq, "someSecret", { expiresIn: "24h" });
    res.status(200).json({ auth: true, token });
  } else {
    res.status(401).json({ auth: false });
  }
});

let conectedUsers = [];

io.on("connection", socket => {
  socket.on("userLogs", payload => {
    const { token } = payload;
    const userLogin = jwt.decode(token).login;
    conectedUsers.push({
      login: userLogin,
      token: token,
      connect: "connected"
    });
    socket.broadcast.emit("newUser", { user: userLogin, conectedUsers });
    socket.emit("newUser", { user: userLogin, conectedUsers });
    socket.on("disconnect", function() {
      console.log("Got disconnect!");
      const disconnectedUserToken = token;
      conectedUsers = conectedUsers.filter(function(obj) {
        return obj.token !== disconnectedUserToken;
      });
      socket.broadcast.emit("userDisconected", {
        user: userLogin,
        token: token,
        conectedUsers,
        connect: "disconected"
      });
    });
  });
  socket.on("userProgress", payload => {
    let userToken = payload.token;
    let progress = payload.progressPercentage;
    socket.broadcast.emit("playerProgress", { userToken, progress });
    socket.emit("playerProgress", { userToken, progress });
  });
});
