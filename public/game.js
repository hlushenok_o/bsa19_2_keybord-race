window.onload = () => {
  const jwt = localStorage.getItem("jwt");
  const track = document.querySelector("#track");
  if (!jwt) {
    location.replace("/login");
  } else {
    fetch("/text", {
      method: "GET",
      headers: {
        Authorization: `Bearer ${jwt}`
      }
    })
      .then(response => {
        let res = response.text();
        return res;
      })
      .then(text => {
        track.innerText = text;
      })
      .catch(err => {
        console.log("request went wrong");
      });

    const socket = io.connect("http://localhost:3000");

    const userListElem = document.querySelector("#users-list");
    const userCount = document.querySelector(".users-count");

    const unique = arr => {
      return arr.reduce((unique, o) => {
        if (
          !unique.some(obj => obj.login === o.login && obj.token === o.token)
        ) {
          unique.push(o);
        }
        return unique;
      }, []);
    };

    socket.emit("userLogs", { token: jwt });

    socket.on("newUser", payload => {
      let sockets = payload.conectedUsers;
      var uniqueUsers = unique(sockets);
      userCount.innerHTML = uniqueUsers.length;
      userListElem.innerHTML = "";
      uniqueUsers.forEach(user => {
        let newLi = document.createElement("li");
        let progressBar = document.createElement("div");
        let progress = document.createElement("div");
        progressBar.appendChild(progress);
        //let left = document.createElement("div");
        newLi.dataset.tokenId = user.token;
        newLi.dataset.connect = user.connect;
        newLi.innerHTML = `${user.login}`;
        progressBar.classList.add("progress");
        progress.dataset.tokenId = user.token;
        newLi.appendChild(progressBar);
        userListElem.appendChild(newLi);
      });
    });
    socket.on("userDisconected", payload => {
      let userElem = document.querySelectorAll("#users-list li");
      let sockets = payload.conectedUsers;
      var uniqueUsers = unique(sockets);
      userCount.innerHTML = `${uniqueUsers.length}`;
      userElem.forEach(e => {
        if (e.dataset.tokenId == payload.token) {
          e.dataset.connect = "disconected";
        }
      });
    });

    function wrapSubstring(sourceString, tag, startIndex, endIndex) {
      return (
        sourceString.substring(0, startIndex) +
        "<" +
        tag +
        ">" +
        sourceString.substring(startIndex, endIndex) +
        "</" +
        tag +
        ">" +
        (endIndex ? sourceString.substring(endIndex) : "")
      );
    }

    let charCount = 0;
    document.addEventListener("keypress", e => {
      let trackTextElem = document.querySelector("#track");
      let trackText = document.querySelector("#track").textContent.toString();
      let trackLength = trackText.length;
      let keynum = e.keyCode;
      let pressedChar = String.fromCharCode(keynum);
      let progressBars = document.querySelectorAll(".progress");
      if (pressedChar == trackText.charAt(charCount)) {
        charCount += 1;
        let progressPercentage = Math.round((charCount / trackLength) * 100);
        socket.emit("userProgress", { progressPercentage, token: jwt });
        socket.on("playerProgress", payload => {
          console.log(payload.userToken);
          progressBars.forEach(bar => {
            if (bar.firstChild.dataset.tokenId == payload.userToken) {
              bar.firstChild.style.width = `${payload.progress}%`;
            }
          });
        });
        let text = wrapSubstring(trackText, "span", 0, charCount);
        trackTextElem.innerHTML = text;
        if (charCount >= trackLength) {
          alert("Wiiiin");
        }
      }
    });
  }
};
